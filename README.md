TWBS Stark
==========

The TWBS Stark theme is provided for demonstration purposes; it uses
[TWBS](https://drupal.org/project/twbs)’s default HTML markup and CSS
styles. It can be used as a troubleshooting tool to determine whether
module-related CSS and JavaScript are interfering with a more complex
theme, and can be used by designers interested in studying
[TWBS](https://drupal.org/project/twbs)’s default markup without the
interference of changes commonly made by more complex themes.

To avoid obscuring CSS added to the page by Drupal or a contrib module,
the TWBS Stark theme itself has no styling.

### Special Note

This theme should use together with
[TWBS](https://drupal.org/project/twbs) in order to provide styling as
screenshot provided.

### Author

-   Developed by [Edison Wong](http://drupal.org/user/33940).
-   Sponsored by [PantaRei Design](http://drupal.org/node/1741828).
